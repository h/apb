package main

import (
	"encoding/json"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"path"
	"unicode/utf8"

	"git.rwth-aachen.de/h/apb/mail"
	"git.rwth-aachen.de/h/apb/subjects"
)

var regFunctions = map[string]func(http.ResponseWriter, *http.Request) error{
	"/":             welcome,
	"/search":       search,
	"/tutswap":      tutSwap,
	"/data":         data,
	"/delete":       del,
	"/dodelete":     doDel,
	"/dosearch":     partnerListingSearch,
	"/doswapsearch": tutSwapSearch,
	"/addlisting":   addPartnerListing,
	"/addswap":      addSwapListing,
	"/info":         info,
	"/respond":      respond,
	"/confirm":      confirm,
	"/fuzzysearch":  fuzzysearch,
}

func search(w http.ResponseWriter, r *http.Request) error {
	args := map[string]interface{}{
		"Subjects": subjects.GetSubjects(),
		"Title":    "Suche",
	}
	return render("templates/search.html", args, w, r, "templates/subject_dropdown.html")
}

func tutSwap(w http.ResponseWriter, r *http.Request) error {
	args := map[string]interface{}{
		"Subjects": subjects.GetSubjects(),
		"Title":    "Tutorientausch",
	}
	return render("templates/tutswap.html", args, w, r, "templates/subject_dropdown.html")
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randomRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func errorClosure(f func(http.ResponseWriter, *http.Request) error) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		err := f(w, r)
		if err != nil {
			errorCode := randomRunes(10)
			log.Println("Internal error, code " + errorCode + ": " + err.Error())
			w.Write([]byte("There has been some kind of internal error. The error code is " + errorCode + "\n" +
				"Please write an issue at https://git.rwth-aachen.de/h/apb/issues/new and include the error code!"))
		}
	}
}

// the registerAll function registers all names in regFunctions to the
// error closure of their response functions
func registerAll() {
	for key, value := range regFunctions {
		http.HandleFunc(key, errorClosure(value))
	}
}

func render(f string, args interface{}, w http.ResponseWriter, r *http.Request, other ...string) error {
	tmpl, err := template.ParseFiles(append(other, []string{"templates/template.html", f}...)...)
	if err != nil {
		return err
	}
	return tmpl.ExecuteTemplate(w, "template", args)
}

func renderError(err string, w http.ResponseWriter, r *http.Request) error {
	return render("templates/userinputerror.html", map[string]string{
		"Title": "Fehler",
		"Error": err,
	}, w, r)
}

func renderMessage(msg string, title string, w http.ResponseWriter, r *http.Request) error {
	return render("templates/message.html", map[string]string{
		"Title":   title,
		"Message": msg,
	}, w, r)
}

func respond(w http.ResponseWriter, r *http.Request) error {
	r.ParseForm()
	message := r.FormValue("message")
	email := r.FormValue("email")
	id := r.FormValue("email_id")
	if message == "" || email == "" {
		return renderError("Nachricht oder Email leer", w, r)
	}
	if id == "" {
		return renderError("Email-ID leer (hast du den DOM verändert?)", w, r)
	}
	to, err := mail.GetEmail(id)
	if err != nil {
		return err
	}

	mailURL, err := url.Parse(mail.GetURL())
	if err != nil {
		return err
	}
	mailURL.Path = path.Join(mailURL.Path, "/delete")

	body := "Nachricht von: " + email + "\n\n" + message + "\n\n" +
		"Gesendet durch die Abgabepartnerboerse.\nWenn du fuendig geworden " +
		"bist, bitte loesche deinen Eintrag:\n" + mailURL.String()

	err = mail.Send(to, "Abgabepartnerboerse - Antwort!", body)
	if err != nil {
		return nil
	}

	return renderMessage("Hab die Mail gesendet. :)", "Okay", w, r)
}

func welcome(w http.ResponseWriter, r *http.Request) error {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return nil
	}
	args := map[string]interface{}{
		"Title": "Willkommen",
	}
	return render("templates/welcome.html", args, w, r)
}

func data(w http.ResponseWriter, r *http.Request) error {
	args := map[string]interface{}{
		"Title": "Datenschutzerklärung",
	}
	return render("templates/data.html", args, w, r)
}

func info(w http.ResponseWriter, r *http.Request) error {
	args := map[string]interface{}{
		"Title":         "Info",
		"VersionNumber": version,
	}
	return render("templates/info.html", args, w, r)
}

func fuzzysearch(w http.ResponseWriter, r *http.Request) error {
	err := r.ParseForm()
	if err != nil {
		return err
	}
	query := r.FormValue("term")
	if query == "" || utf8.RuneCountInString(query) > 50 {
		_, err := w.Write([]byte("{}"))
		return err
	}
	results := subjects.FuzzySearch(query)
	toSend, err := json.Marshal(results)
	if err != nil {
		return err
	}
	_, err = w.Write(toSend)
	return err
}
