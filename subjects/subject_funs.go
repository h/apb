// Package subjects manages the subject list
package subjects

import (
	"math"
	"sort"
	"strings"
)

// HasSubject returns true iff this subject is included in the list
func HasSubject(subject string) bool {
	for _, s := range subjects {
		if s == subject {
			return true
		}
	}
	return false
}

// GetSubjects returns the full list of subjects
func GetSubjects() []string {
	return subjects
}

const numFuzzyResults = 10

// FuzzySearch searches for subjects using a query, with a Wagner-Fisher
// algorithm (used for the autocomplete function)
func FuzzySearch(query string) []string {
	// We want to search for words with a low Wagner-Fisher score.
	// So we have a list of winners with words and scores.

	type winner struct {
		score int
		word  string
	}

	var winners [numFuzzyResults]winner
	// All winners start out with MaxInt64 as score
	for i := 0; i < len(winners); i++ {
		winners[i].score = math.MaxInt64
	}
	// If a word has a score lower than or equal as this, it will enter our list
	maxScoreInWinners := math.MaxInt64

	// After this enters our list, re-assess the max score by calling this function
	updateMaxScoreInWinners := func() {
		maxScore := winners[0].score
		for i := 0; i < len(winners); i++ {
			if winners[i].score > maxScore {
				maxScore = winners[i].score
			}
		}
		maxScoreInWinners = maxScore
	}

	// Calculate Wagner-Fisher score for every subject
	for _, s := range subjects {
		score := wagnerFisher(s, query)
		// We have a winner, so add it to the list where the previous max scorer was
		if score <= maxScoreInWinners {
			for i := 0; i < len(winners); i++ {
				if winners[i].score == maxScoreInWinners {
					winners[i].score = score
					winners[i].word = s
					break
				}
			}
			// and recalculate max score
			updateMaxScoreInWinners()
		}
	}
	// Now sort the list
	winnerSlice := winners[:]
	sort.Slice(winnerSlice, func(i int, j int) bool {
		return winnerSlice[i].score < winnerSlice[j].score
	})

	finalSubjects := make([]string, len(winners))

	for i := 0; i < len(winners); i++ {
		finalSubjects[i] = winners[i].word
	}

	return finalSubjects
}

// wagnerFisher uses the Wagner-Fisher algorithm to score distance between
// the word and the input
func wagnerFisher(word string, input string) int {
	// For easy rune comparison, rune slices are better
	wordRunes := []rune(word)
	inputRunes := []rune(input)
	// This is the distance matrix
	d := make([][]int, len(wordRunes)+1)
	for i := 0; i < len(wordRunes)+1; i++ {
		d[i] = make([]int, len(inputRunes)+1)
	}
	const addingIsBetter = 30
	// Distance of any first string to empty second string
	for i := 0; i < len(wordRunes)+1; i++ {
		d[i][0] = i * addingIsBetter
	}
	// Distance of any second string to empty first string
	for j := 0; j < len(inputRunes)+1; j++ {
		d[0][j] = j * addingIsBetter
	}

	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}

	for j, y := range inputRunes {
		for i, x := range wordRunes {
			if x == y || strings.ToUpper(string(y)) == string(x) {
				d[i+1][j+1] = d[i][j]
			} else if strings.EqualFold(string(x), string(y)) {
				d[i+1][j+1] = d[i][j] + 3
			} else {
				d[i+1][j+1] = min(d[i][j+1]+1, d[i+1][j]+addingIsBetter)
			}
		}
	}
	return d[len(wordRunes)][len(inputRunes)]
}
