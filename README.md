# Abgabepartnerbörse

Die Abgabepartnerbörse läuft momentan unter https://abgabepartner.de.

Kleines Programm in Go. Hier können Studenten der RWTH Aachen

- Abgabepartner finden
- Tutorien tauschen

Die CSS-Styles basieren auf [newkuson](https://git.rwth-aachen.de/h/newkuson).

## Installieren

Clone dieses Repo, installiere Go, einmal `go build` im Ordner und schon ist das executable `apb` da. Für Hilfe, einfach einmal `./apb -h` ausführen.

## Konfigurieren

Konfigurierbar ist momentan nur die Email, über die alle Noreply-Nachrichten verschickt werden. Ein Muster
befindet sich in `email_config_sample.toml`. Fertig konfiguriert kommt diese Datei als `email_config.toml` (!) in den Datenordner (der über `-data` spezifizierte).

## Code beisteuern

Über Merge Requests oder Issues würde ich mich sehr freuen.

## Lizenz

apb befindet sich unter der MIT-Lizenz.
