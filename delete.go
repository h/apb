package main

import (
	"net/http"
	"strconv"
	"strings"

	"git.rwth-aachen.de/h/apb/structs"
	"git.rwth-aachen.de/h/apb/subjects"
)

func del(w http.ResponseWriter, r *http.Request) error {
	args := map[string]interface{}{
		"Title":    "Anzeige löschen",
		"Subjects": subjects.GetSubjects(),
	}
	return render("templates/delete.html", args, w, r, "templates/subject_dropdown.html")
}

func doDel(w http.ResponseWriter, r *http.Request) error {
	if r.Method != "POST" {
		return renderError("Method falsch ("+r.Method+" statt POST)", w, r)
	}
	err := r.ParseForm()
	if err != nil {
		return err
	}
	email := r.FormValue("email")
	if email == "" {
		return renderError("Email leer", w, r)
	}

	subj := r.FormValue("subject")
	if !subjects.HasSubject(subj) {
		return renderError("Dieses Fach existiert nicht. Bitte nutze die Autocomplete-Funktion, "+
			"oder falls du Javascript ablehnst die Liste von akzeptierten Fächern.\n", w, r)
	}

	typ := r.FormValue("type")

	var numDeleted int

	mapToModify, ok := map[string]structs.ListingMap{
		"partner":  &partnerListings,
		"tutorium": &swapListings,
	}[typ]
	if !ok {
		return renderError("type muss entweder partner oder tutorium sein.", w, r)
	}

	numDeleted = 0

	for key, value := range mapToModify.GetMap() {
		if key != subj {
			continue
		}
		i := 0
		for _, listing := range value {
			if !strings.EqualFold(listing.GetEmail(), email) {
				value[i] = listing
				i++
			}
		}
		l := len(value)

		mapToModify.Clear(key)
		for _, v := range value[:i] {
			mapToModify.Push(key, v)
		}
		numDeleted += l - i
	}

	var file string
	switch mapToModify.(type) {
	case *structs.SwapMap:
		file = swapListingsFile
	case *structs.PartnerMap:
		file = partnerListingsFile
	}

	err = saveListings(mapToModify, file)
	if err != nil {
		return err
	}
	if numDeleted == 0 {
		return renderError("Es scheint so, als wäre so ein Eintrag nicht vorhanden.\n"+
			"Ist er doch vorhanden, kontaktiere mich über das Repo.", w, r)
	}
	return renderMessage("Ok, habe "+strconv.Itoa(numDeleted)+" Einträge gelöscht.", "Na dann", w, r)
}
