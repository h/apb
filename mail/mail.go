// Package mail handles sending emails.
package mail

import (
	"errors"
	"io/ioutil"
	"log"
	"math/rand"
	"net/smtp"
	"strconv"

	"github.com/BurntSushi/toml"
)

// This section is to store emails while also keeping them a secret

// random id -> mail
var mailLookup = map[string]string{}

// mail -> random id
var idLookup = map[string]string{}

// GetID returns the id to a given email. If the id
// does not exist yet, it will create the id.
func GetID(email string) string {
	if id, ok := idLookup[email]; ok {
		return id
	}
	id := strconv.Itoa(rand.Int())
	idLookup[email] = id
	mailLookup[id] = email
	return id
}

// GetEmail returns the email to a given id. If the id is invalid,
// it will return an error.
func GetEmail(id string) (string, error) {
	if id, ok := mailLookup[id]; ok {
		return id, nil
	}
	return "", errors.New("this id does not exist")
}

// GetURL gets the email URL (for convenience's sake, it is only configured once, here)
func GetURL() string {
	return config.Location
}

type emailConfig struct {
	Email      string
	Password   string
	SMTPServer string
	Port       string
	Location   string
}

var config emailConfig

// Send sends an email to to, with subject, with body
func Send(to string, subject string, body string) error {
	from := config.Email
	pass := config.Password

	msg := "From: " + "Abgabepartnerboerse <" + from + ">" + "\n" +
		"To: " + to + " <" + to + ">" + "\n" +
		"Subject: " + subject + "\n\n" +
		body

	err := smtp.SendMail(config.SMTPServer+":"+config.Port,
		smtp.PlainAuth("", from, pass, config.SMTPServer),
		from, []string{to}, []byte(msg))

	if err != nil {
		return err
	}

	return nil
}

// LoadConfig loads the config from the specified file path
func LoadConfig(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		config = emailConfig{}
		log.Println("Warning: No email config found. You won't be able to send emails.")
		log.Println("To fix this, please create an email_config.toml in your data folder " +
			"with arguments Email, Password, Location (your apb url, like https://abgabepartner.de) " +
			"SmtpServer (optional, default smtp.gmail.com) and Port (optional, default 587)")
		return nil
	}
	if _, err := toml.Decode(string(data), &config); err != nil {
		return err
	}
	server := "smtp.gmail.com"
	port := "587"
	if config.SMTPServer == "" {
		config.SMTPServer = server
		log.Println("Email config: No SmtpServer specified. Defaulting to " + server)
	}
	if config.Port == "" {
		config.Port = port
		log.Println("Email config: No Port specified. Defaulting to " + port)
	}
	return nil
}
