// Package apbtime handles time functions
package apbtime

import (
	"log"
	"strings"
	"time"
)

// ConvertToTime converts a string in GetFormat() format to a time.Time
func ConvertToTime(timestamp string) (time.Time, error) {
	return time.Parse("02.01.2006 15:04", strings.Replace(timestamp, "um ", "", 1))
}

// GetFormat gets the time format used in the toml files
func GetFormat() string {
	return "02.01.2006 um 15:04"
}

// GetLocation gets the location
func GetLocation() *time.Location {
	loc, err := time.LoadLocation("Europe/Berlin")
	if err != nil {
		log.Fatal(err)
	}
	return loc
}

// GetTime gets the current time in GetFormat() format
func GetTime() string {
	return time.Now().In(GetLocation()).Format(GetFormat())
}
