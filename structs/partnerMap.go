package structs

import (
	"errors"
	"sync"
)

var partnerMutex = &sync.Mutex{}

type PartnerMap map[string][]PartnerListing

func (p *PartnerMap) LenEntries(name string) int {
	partnerMutex.Lock()
	defer partnerMutex.Unlock()
	return len((*p)[name])
}
func (p *PartnerMap) GetEntries(key string) []Listing {
	list := (*p)[key]
	interfaces := make([]Listing, len(list))
	for i := range list {
		interfaces[i] = &list[i]
	}
	return interfaces
}
func (p *PartnerMap) Push(subject string, l Listing) error {
	partnerListing, ok := l.(*PartnerListing)
	if !ok {
		return errors.New("invalid type")
	}
	partnerMutex.Lock()
	defer partnerMutex.Unlock()
	(*p)[subject] = append((*p)[subject], *partnerListing)
	return nil
}
func (p *PartnerMap) GetMap() map[string][]Listing {
	toReturn := map[string][]Listing{}
	for key := range *p {
		toReturn[key] = p.GetEntries(key)
	}
	return toReturn
}
func (p *PartnerMap) Clear(key string) {
	(*p)[key] = nil
}
func (p *PartnerMap) Remove(subject string, toRemove Listing) error {
	partnerListing, ok := toRemove.(*PartnerListing)
	if !ok {
		return errors.New("invalid type")
	}
	partnerMutex.Lock()
	defer partnerMutex.Unlock()
	for i, l := range (*p)[subject] {
		if l == *partnerListing {
			(*p)[subject] = append((*p)[subject][:i], (*p)[subject][i+1:]...)
			return nil
		}
	}
	return errors.New("element to remove does not exist")
}

func NewPartnerMap() ListingMap {
	return &PartnerMap{}
}
