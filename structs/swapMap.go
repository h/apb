package structs

import (
	"errors"
	"sync"
)

var swapMutex = &sync.Mutex{}

type SwapMap map[string][]TutSwapListing

func (p *SwapMap) LenEntries(name string) int {
	return len((*p)[name])
}
func (p *SwapMap) GetEntries(key string) []Listing {
	swapMutex.Lock()
	defer swapMutex.Unlock()
	list := (*p)[key]
	interfaces := make([]Listing, len(list))
	for i := range list {
		interfaces[i] = &list[i]
	}
	return interfaces
}
func (p *SwapMap) Push(subject string, l Listing) error {
	tutSwapListing, ok := l.(*TutSwapListing)
	if !ok {
		return errors.New("invalid type")
	}
	swapMutex.Lock()
	defer swapMutex.Unlock()
	(*p)[subject] = append((*p)[subject], *tutSwapListing)
	return nil
}
func (p *SwapMap) GetMap() map[string][]Listing {
	toReturn := map[string][]Listing{}
	for key := range *p {
		toReturn[key] = p.GetEntries(key)
	}
	return toReturn
}
func (p *SwapMap) Clear(key string) {
	(*p)[key] = nil
}
func (p *SwapMap) Remove(subject string, toRemove Listing) error {
	tutSwapListing, ok := toRemove.(*TutSwapListing)
	if !ok {
		return errors.New("invalid type")
	}
	swapMutex.Lock()
	defer swapMutex.Unlock()
	for i, l := range (*p)[subject] {
		if l == *tutSwapListing {
			(*p)[subject] = append((*p)[subject][:i], (*p)[subject][i+1:]...)
			return nil
		}
	}
	return errors.New("element to remove does not exist")
}

func NewSwapMap() ListingMap {
	return &SwapMap{}
}

type ToSend struct {
	EmailID string
	Listing Listing
}
