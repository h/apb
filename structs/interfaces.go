// Package structs manages the data structures
package structs

import (
	"net/http"
)

type Listing interface {
	GetEmail() string
	IsConfirmed() bool
	GetPostTime() string
	SetEmail(string)
	SetConfirmed(bool)
	BuildFromForm(*http.Request) string
}

type ListingMap interface {
	Push(string, Listing) error
	Clear(string)
	Remove(string, Listing) error
	LenEntries(string) int
	GetEntries(key string) []Listing
	GetMap() map[string][]Listing
}
