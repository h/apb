package structs

import (
	"net/http"

	"git.rwth-aachen.de/h/apb/apbtime"
	"git.rwth-aachen.de/h/apb/subjects"
)

// tutSwapListing represents a tutorium swap listing.
type TutSwapListing struct {
	Email     string
	From      string
	To        string
	Message   string
	PostTime  string
	Confirmed bool
}

func (p *TutSwapListing) GetEmail() string {
	return p.Email
}

func (p *TutSwapListing) IsConfirmed() bool {
	return p.Confirmed
}

func (p *TutSwapListing) GetPostTime() string {
	return p.PostTime
}

func (p *TutSwapListing) SetEmail(email string) {
	p.Email = email
}

func (p *TutSwapListing) SetConfirmed(confirmed bool) {
	p.Confirmed = confirmed
}

func (p *TutSwapListing) BuildFromForm(r *http.Request) string {
	email := r.FormValue("email")
	from := r.FormValue("from")
	to := r.FormValue("to")
	message := r.FormValue("message")
	agreeStr := r.FormValue("agree")
	subject := r.FormValue("subject")
	if from == "" || to == "" {
		return "Von oder zu Tutorium leer"
	}
	if email == "" {
		return "Email leer"
	}
	if agreeStr != "on" {
		return "Du musst den Bedingungen zustimmen"
	}
	if !subjects.HasSubject(subject) {
		return "Das angegebene Fach existiert nicht"
	}
	p.Email = email
	p.From = from
	p.To = to
	p.Message = message
	p.PostTime = apbtime.GetTime()
	p.Confirmed = false
	return ""
}

func NewTutSwapListing() Listing {
	return &TutSwapListing{}
}
