package structs

import (
	"net/http"
	"strconv"

	"git.rwth-aachen.de/h/apb/apbtime"
	"git.rwth-aachen.de/h/apb/subjects"
)

// PartnerListing represents a tutorium partner listing.
type PartnerListing struct {
	Tutorium  string
	Email     string
	NumPeople int
	Message   string
	PostTime  string
	Confirmed bool
}

func (p *PartnerListing) GetEmail() string {
	return p.Email
}
func (p *PartnerListing) IsConfirmed() bool {
	return p.Confirmed
}
func (p *PartnerListing) GetPostTime() string {
	return p.PostTime
}
func (p *PartnerListing) SetEmail(email string) {
	p.Email = email
}
func (p *PartnerListing) SetConfirmed(confirmed bool) {
	p.Confirmed = confirmed
}
func (p *PartnerListing) BuildFromForm(r *http.Request) string {
	email := r.FormValue("email")
	numStudentsStr := r.FormValue("num_students")
	message := r.FormValue("message")
	agreeStr := r.FormValue("agree")
	subject := r.FormValue("subject")
	tut := r.FormValue("tutorium")
	if email == "" {
		return "Email leer"
	}
	numStudents, err := strconv.Atoi(numStudentsStr)
	if err != nil {
		return "Anzahl Studenten muss eine Integer-Zahl sein"
	}
	if numStudents < 1 {
		return "Anzahl Studenten muss >= 1 sein"
	}
	if agreeStr != "on" {
		return "Du musst den Bedingungen zustimmen"
	}
	if !subjects.HasSubject(subject) {
		return "Das angegebene Fach existiert nicht"
	}
	if tut != "" {
		intTut, err := strconv.Atoi(tut)
		if err != nil {
			return "Das Tutorium muss eine Integer-Zahl oder leer sein"
		}
		if intTut < 0 {
			return "Das Tutorium muss größer oder gleich null sein"
		}
	}
	p.Email = email
	p.NumPeople = numStudents
	p.Message = message
	p.PostTime = apbtime.GetTime()
	p.Confirmed = false
	p.Tutorium = tut
	return ""
}

func NewPartnerListing() Listing {
	return &PartnerListing{}
}

// OtherTut is the struct for representing "in other tutoriums, same subject" messages
type OtherTut struct {
	Name    string
	ID      string
	Subject string
}
