# 1st stage: build go app
FROM golang:1.12 as builder

WORKDIR /go/src/git.rwth-aachen.de/h/apb/

COPY . .

ENV GO111MODULE=on 

RUN go get -d -v ./...
RUN go install -v ./...

RUN CGO_ENABLED=0 GOOS=linux go build -a -i -buildmode default -installsuffix cgo -o apb .

# 2nd stage: bundle alpine image
FROM alpine:latest

RUN apk --no-cache add tzdata ca-certificates

WORKDIR /root/

COPY --from=builder /go/src/git.rwth-aachen.de/h/apb .

ENTRYPOINT ["./apb"]
