package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"

	"git.rwth-aachen.de/h/apb/mail"
	"git.rwth-aachen.de/h/apb/structs"
	"github.com/BurntSushi/toml"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var confirmationsMutex = &sync.Mutex{}
var confirmationsPath string

type confirmationType int

const (
	// Addition represents the addition of a confirmation
	Addition = iota
	// Deletion represents the deletion of a confirmation
	Deletion
)

type confirmation struct {
	Email   string
	Subject string
	Type    confirmationType
}

type confirmationMap map[string]confirmation

var confirmations confirmationMap

func (c confirmationMap) putNewEntry(conf confirmation) error {
	confirmationsMutex.Lock()
	defer confirmationsMutex.Unlock()
	r := strconv.Itoa(rand.Int())
	c[r] = conf
	saveConfirmations(confirmationsPath)
	return mail.Send(conf.Email, "Email bestaetigen",
		"Hey, bitte bestaetige deine Email: "+mail.GetURL()+"confirm?id="+r)

}

func confirm(w http.ResponseWriter, r *http.Request) error {
	confirmationsMutex.Lock()
	defer confirmationsMutex.Unlock()
	r.ParseForm()
	id := r.FormValue("id")
	if id == "" {
		return renderError("Die ID ist leer.", w, r)
	}
	confirm, ok := confirmations[id]
	if !ok {
		return renderError("Dieser Link ist ungültig. Das tut uns leid :(. Bitte probiere es noch einmal.", w, r)
	}

	stuff := []structs.ListingMap{&partnerListings, &swapListings}
	files := map[structs.ListingMap]string{
		stuff[0]: partnerListingsFile,
		stuff[1]: swapListingsFile,
	}

	confirmedNum := 0
	for _, s := range stuff {
		for subject, list := range s.GetMap() {
			for _, listing := range list {
				if listing.GetEmail() == confirm.Email && subject == confirm.Subject && !listing.IsConfirmed() {
					listing.SetConfirmed(true)
					delete(confirmations, id)
					saveConfirmations(confirmationsPath)
					saveListings(s, files[s])
					confirmedNum++
				}
			}
		}
	}

	if confirmedNum > 0 {
		return renderMessage(strconv.Itoa(confirmedNum)+" Anzeige(n) sind jetzt online.", "Hat geklappt", w, r)
	}

	return renderMessage("Diese Anzeige gibt es nicht", "Seltsam", w, r)
}

func loadConfirmations(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		confirmations = confirmationMap{}
		err = saveConfirmations(file)
		log.Println("There was no confirmations file, so I created a new one")
		return err
	}
	if _, err := toml.Decode(string(data), &confirmations); err != nil {
		return err
	}
	return nil
}

func saveConfirmations(file string) error {
	var buf bytes.Buffer
	encoder := toml.NewEncoder(&buf)
	err := encoder.Encode(confirmations)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(file, buf.Bytes(), 0644)
	if err != nil {
		return err
	}
	return nil
}
