package main

import (
	"flag"
	"log"
	"net/http"
	"path"
	"strconv"
	"time"

	"git.rwth-aachen.de/h/apb/mail"
	"git.rwth-aachen.de/h/apb/structs"
)

var version = "2.4.1"

var partnerListingsFile string
var swapListingsFile string
var versionFile string

func main() {
	const DefaultPort = 8080
	portPtr := flag.String("port", strconv.Itoa(DefaultPort),
		"The port the server runs on")
	ipStrPtr := flag.String("ip", "0.0.0.0", "The ip the server runs on")
	dataPath := flag.String("data", "files/", "The path of your data files")
	flag.Parse()

	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		r.Header.Set("Cache-Control", "no-cache")
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	partnerListingsFile = path.Join(*dataPath, "partner.toml")
	err := loadListings(structs.NewPartnerMap, &partnerListings, partnerListingsFile)
	if err != nil {
		log.Fatal(err)
	}

	swapListingsFile = path.Join(*dataPath, "swap.toml")
	err = loadListings(structs.NewSwapMap, &swapListings, swapListingsFile)
	if err != nil {
		log.Fatal(err)
	}

	emailConfigPath := path.Join(*dataPath, "email_config.toml")
	err = mail.LoadConfig(emailConfigPath)
	if err != nil {
		log.Fatal(err)
	}

	confirmationsPath = path.Join(*dataPath, "confirmations.toml")
	err = loadConfirmations(confirmationsPath)
	if err != nil {
		log.Fatal(err)
	}

	backup(path.Join(*dataPath, "/backups/"))

	clearTicker := time.NewTicker(1 * time.Hour)
	go func() {
		for range clearTicker.C {
			err := clearOldListings(&partnerListings)
			if err != nil {
				log.Fatal(err)
			}

			err = clearOldListings(&swapListings)
			if err != nil {
				log.Fatal(err)
			}
		}
	}()
	err = clearOldListings(&partnerListings)
	if err != nil {
		log.Fatal(err)
	}

	err = clearOldListings(&swapListings)
	if err != nil {
		log.Fatal(err)
	}

	registerAll()

	log.Printf("Starting server at port %s and IP %s\n", string(*portPtr), string(*ipStrPtr))
	log.Fatal(http.ListenAndServe(*ipStrPtr+":"+string(*portPtr), nil))
}
