package main

import (
	"errors"
	"html/template"
	"net/http"
	"strconv"

	"git.rwth-aachen.de/h/apb/mail"
	"git.rwth-aachen.de/h/apb/structs"
	"git.rwth-aachen.de/h/apb/subjects"
)

func tutSwapSearch(w http.ResponseWriter, r *http.Request) error {
	return listingSearch(&swapListings, w, r)
}

func partnerListingSearch(w http.ResponseWriter, r *http.Request) error {
	return listingSearch(&partnerListings, w, r)
}

func searchEntries(l structs.ListingMap, subj string, tut string) []structs.Listing {
	found := []structs.Listing{}
	for _, listing := range l.GetEntries(subj) {
		if l, ok := listing.(*structs.PartnerListing); tut != "" && ok {
			if l.Tutorium != tut {
				continue
			}
		}
		found = append(found, listing)
	}
	return found
}

// listingsSearch is the general function that searches for listings
// and sends the correct http templated response
func listingSearch(l structs.ListingMap, w http.ResponseWriter, r *http.Request) error {
	err := r.ParseForm()
	if err != nil {
		return err
	}
	subj := r.FormValue("subject")

	if !subjects.HasSubject(subj) {
		return renderError("Dieses Fach existiert nicht. Bitte nutze die Autocomplete-Funktion, "+
			"oder falls du Javascript ablehnst die Liste von akzeptierten Fächern.\n", w, r)
	}

	tut := r.FormValue("tutorium")
	if tut != "" {
		intTut, err := strconv.Atoi(tut)
		if err != nil {
			return renderError("Das Tutorium muss eine Integer-Zahl oder leer sein", w, r)
		}
		if intTut < 0 {
			return renderError("Das Tutorium muss größer oder gleich null sein", w, r)
		}
	}

	found := searchEntries(l, subj, tut)

	var resultFolderName string
	var foundTemplate string

	switch l.(type) {
	case *structs.PartnerMap:
		resultFolderName = "search"
		foundTemplate = "templates/found.html"
	case *structs.SwapMap:
		resultFolderName = "swap"
		foundTemplate = "templates/tutswap_found.html"
	default:
		return errors.New("invalid type of ListingMap")
	}

	var resultTemplate string

	if len(found) == 0 {
		resultTemplate = "templates/" + resultFolderName + "/result_none.html"
	} else {
		resultTemplate = "templates/" + resultFolderName + "/result_some.html"
	}

	resultsToIds := []structs.ToSend{}
	for _, r := range found {
		if !r.IsConfirmed() {
			continue
		}
		resultsToIds = append(resultsToIds, structs.ToSend{
			EmailID: mail.GetID(r.GetEmail()),
			Listing: r,
		})
	}

	tmpl, err := template.ParseFiles("templates/template.html", foundTemplate,
		"templates/disclaimer.html", "templates/respond_box.html", resultTemplate)
	if err != nil {
		return err
	}

	var subjectText string
	if tut == "" {
		subjectText = subj + ": Alle Anzeigen"
	} else {
		subjectText = subj + ": Tutorium " + tut
	}

	return tmpl.ExecuteTemplate(w, "template", map[string]interface{}{
		"FoundListings":  resultsToIds,
		"Subject":        subj,
		"Title":          "Resultate",
		"SubjectText":    subjectText,
		"Tutorium":       tut,
		"NextDeleteDate": nextDelDate.time.Format("2. Jan 2006"),
	})
}

/*
	Add a listing
*/

// addPartnerListing is the http function to add a new partner listing
func addPartnerListing(w http.ResponseWriter, r *http.Request) error {
	return addListing(structs.NewPartnerListing, &partnerListings, partnerListingsFile, w, r)
}

// addSwapListing is the http function to add a new swap listing
func addSwapListing(w http.ResponseWriter, r *http.Request) error {
	return addListing(structs.NewTutSwapListing, &swapListings, swapListingsFile, w, r)
}

// addListing is the general function that adds a new listing
// and sends the correct http templated response
func addListing(newFunc func() structs.Listing, listingMap structs.ListingMap, saveFile string, w http.ResponseWriter, r *http.Request) error {
	if r.Method != "POST" {
		return renderError("Falsche Methode ("+r.Method+", nicht POST)", w, r)
	}
	err := r.ParseForm()
	if err != nil {
		return err
	}
	listing := newFunc()
	errStr := listing.BuildFromForm(r)
	if errStr != "" {
		return renderError(errStr, w, r)
	}
	subj := r.FormValue("subject")
	if !subjects.HasSubject(subj) {
		return renderError("Dieses Fach gibt es nicht", w, r)
	}

	listingMap.Push(subj, listing)

	err = saveListings(listingMap, saveFile)
	if err != nil {
		return err
	}

	err = confirmations.putNewEntry(confirmation{
		Email:   listing.GetEmail(),
		Subject: subj,
		Type:    Addition,
	})
	if err != nil {
		return err
	}

	return renderMessage("Wir haben dir eine Email geschickt. Bitte schau auch in deinem Spam-Fach nach.",
		"Ok!", w, r)
}
