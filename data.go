package main

import (
	"bytes"
	"io/ioutil"
	"log"

	"git.rwth-aachen.de/h/apb/structs"
	"github.com/BurntSushi/toml"
)

var partnerListings structs.PartnerMap
var swapListings structs.SwapMap

// loadListings tries to load the listings from file.
// If this file does not exist, it will create a new one.
func loadListings(newFunc func() structs.ListingMap, into structs.ListingMap, file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		err = saveListings(newFunc(), file)
		log.Println("There was no " + file + " file, so I created a new one.")
		if err != nil {
			return err
		}
	}
	if _, err := toml.Decode(string(data), into); err != nil {
		return err
	}
	return nil
}

func saveListings(s structs.ListingMap, filename string) error {
	var buf bytes.Buffer
	encoder := toml.NewEncoder(&buf)
	err := encoder.Encode(s)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, buf.Bytes(), 0644)
	if err != nil {
		return err
	}
	return nil
}
