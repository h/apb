package main

import (
	"log"
	"time"

	"git.rwth-aachen.de/h/apb/subjects"

	"git.rwth-aachen.de/h/apb/apbtime"
	"git.rwth-aachen.de/h/apb/structs"
)

// This function log.Fatals because it is only called on initialising the semesterEndDates.
func toTime(semesterEndDate string) time.Time {
	t, err := time.ParseInLocation("January 2, 2006", semesterEndDate, apbtime.GetLocation())
	if err != nil {
		log.Fatal(err)
	}
	return t
}

type semesterEndDate struct {
	name string
	time time.Time
}

var semesterEndDates = []semesterEndDate{
	semesterEndDate{"Summer Semester 2018", toTime("July 20, 2018")},
	semesterEndDate{"Winter Semester 2018/2019", toTime("February 1, 2019")},
	semesterEndDate{"Summer Semester 2019", toTime("July 12, 2019")},
	semesterEndDate{"Winter Semester 2019/2020", toTime("January 31, 2020")},
	semesterEndDate{"Summer Semester 2020", toTime("July 17, 2020")},
	semesterEndDate{"Winter Semester 2020/2021", toTime("January 29, 2021")},
	semesterEndDate{"Summer Semester 2021", toTime("July 23, 2021")},
	semesterEndDate{"Winter Semester 2021/2022", toTime("January 28, 2022")},
	semesterEndDate{"Summer Semester 2022", toTime("July 15, 2022")},
	semesterEndDate{"Winter Semester 2022/2023", toTime("January 27, 2023")},
	semesterEndDate{"Summer Semester 2023", toTime("July 4, 2023")},
	semesterEndDate{"Winter Semester 2023/2024", toTime("February 2, 2024")},
	semesterEndDate{"Forever", toTime("February 20, 9999")},
}

var nextDelDate semesterEndDate

func clearOldListings(listingMap structs.ListingMap) error {
	var lastSemesterEndDate semesterEndDate
	currTime := time.Now().In(apbtime.GetLocation())
	for _, date := range semesterEndDates {
		if currTime.Before(date.time) {
			nextDelDate = date
			break
		}
		lastSemesterEndDate = date
	}
	deletedStuff := false
	for _, subject := range subjects.GetSubjects() {
		for _, entry := range listingMap.GetEntries(subject) {
			t, err := apbtime.ConvertToTime(entry.GetPostTime())
			if err != nil {
				return err
			}
			if t.Before(lastSemesterEndDate.time) {
				deletedStuff = true
				err := listingMap.Remove(subject, entry)
				if err != nil {
					return err
				}
			}
		}
	}
	if deletedStuff {
		log.Println("Deleted listings because " + lastSemesterEndDate.name + " ended")
	}
	return nil
}
