package main

import (
	"log"
	"os"
	"path"
	"time"
)

func backup(backupPath string) {
	channel := time.Tick(12 * time.Hour)
	os.MkdirAll(backupPath, os.FileMode(0777))
	go func() {
		for {
			folderName := time.Now().Format("Jan_02_15_04_05_2006")
			log.Println("Making backup " + folderName)
			os.MkdirAll(path.Join(backupPath, folderName), os.FileMode(0777))
			err := saveListings(&partnerListings, path.Join(backupPath, folderName, "partner.toml"))
			if err != nil {
				log.Println(err.Error())
			}
			err = saveListings(&swapListings, path.Join(backupPath, folderName, "swap.toml"))
			if err != nil {
				log.Println(err.Error())
			}
			os.Link(versionFile, path.Join(backupPath, folderName, "version.txt"))
			<-channel
		}
	}()
}
